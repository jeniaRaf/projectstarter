const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
//var path = require('path');

const prod = process.argv.indexOf('-p') !== -1;
var projectMode = prod ? `production`: 'development' // production , test

module.exports = {
  module: {
    rules: [
        {
            enforce: "pre",
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "eslint-loader",
            options: {
                emitError: true,
                quiet: false, // Loader will process and report errors only and ignore warnings if this option is set to true
            }
        },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
        {
            test: /\.less$/,
            loader: "style-loader!css-loader!less-loader?strictMath&noIeCompat"
        },

        {
            test: /.(png|gif|jpg|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/,
            loader: 'url-loader?limit=100000'
        },
        // https://github.com/bholloway/resolve-url-loader fix SCSS problem
        {
            test   : /\.css$/,
            loaders: ['style-loader', 'css-loader?url=false']
        }, {
            test   : /\.scss$/,
            loaders: ['style-loader', 'css-loader?url=false' , 'sass-loader?sourceMap']
        },
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }),
      new CopyWebpackPlugin([ { from: 'src/assets', to: 'assets' } ]), // copy to project
  ]
};


if(!prod){
    // add map file cheap-module-source-map,source-map,eval-source-map , source-map
    module.exports.devtool = "source-map";
}
// , new CopyWebpackPlugin([{from: './src/assets', to: './dist/assets'}])