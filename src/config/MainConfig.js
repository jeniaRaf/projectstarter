let ROOT_URL = ""
let isDebug = false;

if (location.hostname === "localhost"){
    // localhost debug server
    ROOT_URL = 'http://localhost:3006';
    isDebug = true;
}

export const MAIN_CONFIG = {
    ROOT_URL,
    isDebug
};