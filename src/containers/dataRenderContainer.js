import React , {Component,Fragment} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {requestServerData,updateDataAction} from '../actions/index'
import _ from 'lodash'


import '../style/dataRenderContainer.css'
import '../style/sassExample.scss'

function showData(name,data){
    return(
        <Fragment>
            <br/>
            {name}: {JSON.stringify(data)}
        </Fragment>
    )
}



class myComponent extends Component{
    componentDidMount(){
        this.props.requestServerData();
    }
    updateData = () => {
        // clone object
        let newServerData= _.cloneDeep(this.props.serverData);
        // find camera index by id
        let index = _.findIndex(newServerData, { key: "2" });
        // set new Data
        newServerData[index].value = `new time ${new Date().getTime()}`
        this.props.updateDataAction(newServerData)
    }
    render(){
        if(!this.props.serverData){
            return <div>Loading...</div>
        }
        const {serverData , serverDataBasicReducer , serverDataMap , serverUpdatbleData} = this.props;
        return(
           <div>
               {showData('serverData',serverData) }
               {showData('serverDataBasicReducer',serverDataBasicReducer) }
               {showData('serverDataMap',serverDataMap) }
               {showData('serverUpdatbleData',serverUpdatbleData) }\
               <br/>
                <br/>
               <button className='button' onClick={this.props.requestServerData}>request data from server</button>
               <br/>
               <button className='button' onClick={this.updateData}>update redux store data of serverUpdatbleData</button>
           </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({requestServerData , updateDataAction},dispatch)
}


function mapStateToProps({serverData , serverDataBasicReducer , serverDataMap , serverUpdatbleData}) {
    return  {serverData , serverDataBasicReducer , serverDataMap , serverUpdatbleData}
}

export default connect(mapStateToProps,mapDispatchToProps)(myComponent)