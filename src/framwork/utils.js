export const deepStaticCopy = (data) => {
    return JSON.parse(JSON.stringify(data));
};