import {
    ACTIONS_LIST
} from '../constants';

import Reducer from './basicReducer'
import _ from 'lodash'

// simple basic comon reducer
export function serverDataReducer(state = '' , action) {
    switch (action.type){
        case  ACTIONS_LIST.SERVER_DATA:
            return  action.payload.data
    }
    return state;
}

// basic use of global reducer
export const serverDataBasicReducer  = Reducer(ACTIONS_LIST.SERVER_DATA)

// updatble localy reducer
export function serverDataUpdatbleReducer(state = '' , action) {
    switch (action.type){
        case  ACTIONS_LIST.SERVER_DATA:
            return  action.payload.data
        case  ACTIONS_LIST.UPDATE_SERVER_DATA_LOCAL:
            return  action.payload
    }
    return state;
}



/*
reducer recive:
   [{"key":"1","value":1},{"key":"2","value":2},{"key":"3","value":3}]
return:
  {"1":{"key":"1","value":1},"2":{"key":"2","value":2},"3":{"key":"3","value":3}}
*/


export function serverDataReducerMap(state = null , action) {
    switch (action.type){
        case  ACTIONS_LIST.SERVER_DATA:
            return  _.keyBy(action.payload.data, 'key');
    }
    return state;
}