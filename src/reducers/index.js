import { combineReducers } from 'redux';
import {serverDataReducer,serverDataBasicReducer,serverDataReducerMap,serverDataUpdatbleReducer} from './MainReducers';


const rootReducer = combineReducers({
    serverData:serverDataReducer,
    serverDataBasicReducer:serverDataBasicReducer,
    serverDataMap:serverDataReducerMap,
    serverUpdatbleData:serverDataUpdatbleReducer
});

export default rootReducer;