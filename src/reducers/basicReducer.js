// recive Action and defaultPayload data

export default (actionName,defaultPayload = null) => (state = null , action) => {
    if(action.type == actionName){
       try{
           action.payload.request.responseURL
           return action.payload.data;
       }catch(e){
           return action.payload;
       }
    }
    if(state == null){
        return defaultPayload;
    }
    return state;
}