// import actionconsts
import {ACTIONS_LIST} from "../constants/MainActions";
// import basic config
import {MAIN_CONFIG} from '../config/index'
// http ajax
import axios from 'axios';

// good practic for create vars from objects
const {ROOT_URL} = MAIN_CONFIG;

// sample server redux action request
export function requestServerData() {
    const url = `${ROOT_URL}/data`
    const request = axios.get(url);
    return{
        type: ACTIONS_LIST.SERVER_DATA,
        payload: request
    }
}

export function updateDataAction(newData) {
    return{
        type: ACTIONS_LIST.UPDATE_SERVER_DATA_LOCAL,
        payload: newData
    }
}