/* eslint-disable react/jsx-key */


import React , {Fragment} from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
// init promise
// pause action and wait recive respons from proomse and then pas it to reducers
// on payload on event
import ReduxProomse from 'redux-promise'

import reducers from './reducers';
import ErrorBoundary from './framwork/ErrorBoundary'

const createStoreWithMiddleware = applyMiddleware(ReduxProomse)(createStore);
const store = createStoreWithMiddleware(reducers , window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

import './style/reset200802.css'

import App from './containers/app'

ReactDOM.render(
    <ErrorBoundary>
        <Provider store={store}>
            <Fragment>
                <App/>
            </Fragment>
        </Provider>
    </ErrorBoundary>
    ,  document.getElementById("app"));
