# Demo
> Video manager


## Development

```bash
npm i 
npm run start:server    // run JSON Server
npm run start
```

## Build

```bash
npm build  // have source map
npm build:prodaction
```
## Live Tamplet webstorm

MyLiveTemplates.js




## Dependencies


| Dependencies |  description |
|-----|---------------|
| [webpack 4](https://webpack.js.org/)| [webpack 4, is FAST (up to 98% faster)!](https://medium.com/webpack/webpack-4-released-today-6cdb994702d4)
| [ES6](http://es6-features.org/#Constants)/[7](https://medium.freecodecamp.org/ecmascript-2016-es7-features-86903c5cab70) | [ES7 Decorators in ReactJS](https://medium.com/@jihdeh/es7-decorators-in-reactjs-22f701a678cd) 
| [React  16.3](https://reactjs.org/) | [What’s new in React 16.3](https://www.infoworld.com/article/3228113/javascript/whats-new-in-react-163-javascript-ui-library.html)   , [New lifecycles ](https://reactjs.org/blog/2018/03/29/react-v-16-3.html)   ,   [Portal](https://hackernoon.com/using-a-react-16-portal-to-do-something-cool-2a2d627b0202)
| [REDUX](https://github.com/reactjs/redux) | Redux is a predictable state container for JavaScript apps 
| [react-redux](https://github.com/reactjs/react-redux) | Official React bindings for Redux
| [redux-promise](https://github.com/redux-utilities/redux-promise) | The middleware returns a promise to the caller so that it can wait for the operation to finish before continuing
|  [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)   | chrome extension
| [SCSS](https://sass-lang.com/)  |  CSS with superpowers
| [ESLint](https://eslint.org/) | ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code
| [React Hot Loader](http://gaearon.github.io/react-hot-loader/getstarted/) | React Hot Loader is a plugin that allows React components to be live reloaded without the loss of state
| [axios](https://github.com/axios/axios) | Promise based HTTP client for the browser and node.js
| [JSON Server](https://github.com/typicode/json-server) |  Get a full fake REST API with zero coding in less than 30 seconds
| [reset200802.css](https://meyerweb.com/eric/tools/css/reset/reset200802.css) | Reset basic html css './style/reset200802.css'


Ivgeny Rafalovich - gwmaster.gwm@gmail.com

