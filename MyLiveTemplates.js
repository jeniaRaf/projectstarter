//config -> live templates

//sub folder 'REACT'



//-------   rc - Create React basic class  -----------

import React , {Component} from 'react';

export default class $COMPONENT$ extends Component{
    render(){
        return(
            <div></div>
        )
    }
}


//-------  rop - Redux one param  --------

import React , {Component} from 'react';
import {connect} from 'react-redux'

class $COMPONENT$ extends Component{
    render(){
        return(
           <div></div>
        )
    }
}

// if one value returned
function mapStateToProps({$VAR$}) {
    return  {$VAR$}
}

export default connect(mapStateToProps)($COMPONENT$)


//-----  rmp - Redax multiple params ------

import React , {Component} from 'react';
import {connect} from 'react-redux'

class $COMPONENT$ extends Component{
    render(){
        return(
           <div></div>
        )
    }
}

function mapStateToProps(state) {
    return  {$VAR$ : state.$VAR$}
}

export default connect(mapStateToProps)($COMPONENT$)


//----- rpf- Redux params functions

import React , {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {$FUNCTION_NAME$} from '../actions/index'


class myComponent extends Component{
    render(){
        return(
           <div></div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({$FUNCTION_NAME$},dispatch)
}


function mapStateToProps({$VAR$}) {
    return  {$VAR$}
}

export default connect(mapStateToProps,mapDispatchToProps)(myComponent)





